package com.example.mcdonald.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mcdonald.R;
import com.example.mcdonald.common.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AlertTool {
    private final static String TAG = "AlertTool";
    /**
     * 訊息視窗，只有確認按鈕，按下確認後只會關閉Dialog
     *
     * @param context context
     * @param title    視窗標題
     * @param message  訊息
     * @return AlertDialog 物件
     */
    public static AlertDialog getAlertDialog (Context context, String title, String message) {
        //產生一個Builder物件
        Builder builder = new Builder(context);

        //設定Dialog的標題
        builder.setTitle(title);

        //設定Dialog的內容
        builder.setMessage(message);

        //設定Positive按鈕資料
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setCancelable(false);

        return builder.create();
    }

    /**
     * 訊息視窗，只有確認按鈕，需要設定Positive的onClickListener
     *
     * @param context context
     * @param title    視窗標題
     * @param message  訊息
     * @param positive 確認按鈕的listener
     * @return AlertDialog 物件
     */
    public static AlertDialog getDialogHasConfirm(Context context, String title, String message, DialogInterface.OnClickListener positive) {
        //產生一個Builder物件
        Builder builder = new Builder(context);

        //設定Dialog的標題
        builder.setTitle(title);

        //設定Dialog的內容
        builder.setMessage(message);

        //設定Positive按鈕資料
        builder.setPositiveButton(context.getString(R.string.ok), positive);

        builder.setCancelable(false);

        return builder.create();
    }

    /**
     * 訊息視窗，有確認和取消按鈕，需要Positive和Negative的onClickListener
     *
     * @param context context
     * @param title    視窗標題
     * @param message  訊息
     * @param positive 確認按鈕的listener
     * @param negative 取消按鈕的listener
     * @return AlertDialog 物件
     */
    public static AlertDialog getDialogHas2Button(Context context, String title, String message, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative) {
        //產生一個Builder物件
        Builder builder = new Builder(context);

        //設定Dialog的標題
        builder.setTitle(title);

        //設定Dialog的內容
        builder.setMessage(message);

        //設定Positive按鈕資料
        builder.setPositiveButton(context.getString(R.string.ok), positive);

        //設定Negative按鈕資料
        builder.setNegativeButton(context.getString(R.string.cancel), negative);

        builder.setCancelable(false);

        return builder.create();
    }

    public static AlertDialog radioDialog (Context context, int title, int array, TextView tv) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final String [] regionItems = builder.getContext().getResources().getStringArray(array);
        final TextView textView = tv;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setSingleChoiceItems(regionItems, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                textView.setText(regionItems[item]);
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static AlertDialog cityDialog (Context context, int title, int array, TextView city, TextView region, final TextView postalCode) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final String [] regionItems = builder.getContext().getResources().getStringArray(array);
        final TextView cityTextView = city;
        final TextView regionTextView = region;
        final TextView postalCodeextView = postalCode;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setSingleChoiceItems(regionItems, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                cityTextView.setText(regionItems[item]);
                regionTextView.setText("");
                postalCodeextView.setText("");

                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static AlertDialog regionAndPostalCodeDialog (Context context, int title, int regionList, int postalCodeList, TextView region, TextView postalCode) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final String [] regionItems = builder.getContext().getResources().getStringArray(regionList);
        final int [] postalCodeItems = builder.getContext().getResources().getIntArray(postalCodeList);
        final TextView regionTextView = region;
        final TextView postalCodeTextView = postalCode;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setSingleChoiceItems(regionItems, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                regionTextView.setText(regionItems[item]);
                if(item != 0) postalCodeTextView.setText(String.valueOf(postalCodeItems[item-1]));
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static AlertDialog RegionDialog (Context context, int title, int array, TextView tv1,TextView tv2) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final String [] items = builder.getContext().getResources().getStringArray(array);
        final String [] postalCode = builder.getContext().getResources().getStringArray(array);
        final TextView textView1 = tv1;
        final TextView textView2 = tv2;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                textView1.setText(items[item]);
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static AlertDialog childDialog (Context context, int title, int array, TextView tv1, LinearLayout layout) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final String [] items = builder.getContext().getResources().getStringArray(array);
        final TextView select = tv1;
        final LinearLayout display = layout;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                select.setText(items[item]);
                String option = select.getText().toString();
                if(option.equals(Constants.HAVE)){
                    Log.i(TAG,"option is "+option );
                    display.setVisibility(View.VISIBLE);
                }else{
                    Log.i(TAG,"option is "+option );
                    display.setVisibility(View.GONE);
                }
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static AlertDialog yearDialog (Context context, int title, TextView tv1) {
        //產生一個Builder物件
        final Builder builder = new Builder(context);
        final TextView select = tv1;
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        final  List<String> year = new ArrayList<>();
        int thisYear = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
        for(int i = 1980; i<= thisYear ;i++){
            year.add(String.valueOf(i));
        }
        final String[]yearList = year.toArray(new String[year.size()]);
        builder.setItems(yearList,new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                select.setText(yearList[item]);
            }
        });
      //  //設定Positive按鈕資料
      //  builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
      //      public void onClick(DialogInterface dialog, int which) {
      //
      //      }
      //  });
//
      //  //設定Negative按鈕資料
      //  builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
      //      public void onClick(DialogInterface dialog, int which) {
      //
      //      }
      //  });

        return builder.create();
    }

    /**
     * 設定錯誤訊息字串
     *
     * @param errMsg    錯誤訊息物件
     * @param appendStr 錯誤訊息
     * @return StringBuilder
     */
    public static StringBuilder editErrMsg(StringBuilder errMsg, String appendStr) {
        if (errMsg.length() != 0) {
            errMsg.append("\n");
        }
        errMsg.append(appendStr);
        return errMsg;
    }
}