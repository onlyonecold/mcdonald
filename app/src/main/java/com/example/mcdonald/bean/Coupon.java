package com.example.mcdonald.bean;

public class Coupon {
    private Integer couponImage;
    private String couponName;

    public Coupon(Integer couponImage,String couponName){
        this.couponImage = couponImage;
        this.couponName = couponName;
    }

    public Integer getCouponImage() {
        return couponImage;
    }

    public void setCouponImage(Integer couponImage) {
        this.couponImage = couponImage;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }
}
