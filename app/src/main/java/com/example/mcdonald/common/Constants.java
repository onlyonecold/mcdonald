package com.example.mcdonald.common;

public class Constants {

    public static final int BASE_1_MIN = 60;
    public static final int WAIT_2_MINS = BASE_1_MIN * 2;
    public static final int WAIT_5_MINS = BASE_1_MIN * 5;

    /**
     *API Parameter
     */
    public static final String PARAMETER_MOBILE = "mobile";
    public static final String PASSWORD = "password";
    public static final String AUTH_CODE = "authCode";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_SUCCESS = "success";

    public static final String JSON_SURNAME = "surname";
    public static final String JSON_NAME = "name";
    public static final String JSON_GENDER = "gender";
    public static final String JSON_BIRTHDAY = "birthday";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_CITY = "city";
    public static final String JSON_REGION = "region";
    public static final String JSON_POSTAL_CODE = "postalCode";
    public static final String JSON_ADDRESS = "address";
    public static final String JSON_MARITAL_STATUS = "maritalStatus";
    public static final String JSON_CHILD = "child";
    public static final String JSON_YOUNGEST_AGE = "youngestAge";
    public static final String JSON_RECEIVE = "receive";
    public static final String JSON_FACEBOOK_ID = "facebookId";
    public static final String JSON_GOOGLE_ID = "googleId";





    public static final String MOBILE_PHONE_VERIFICATION= "09+[0-9]{2}[0-9]{6}";
    public static final String PASSWORD_VERIFICATION= "^(?=.*[a-zA-Z]+)(?=.*\\d+)[a-zA-Z0-9]{6,12}$";
    public static final String EMAIL_VERIFICATION = "^\\w{1,63}@[a-zA-Z0-9]{2,63}\\.[a-zA-Z]{2,63}(\\.[a-zA-Z]{2,63})?$";

    /**
     * ERROR MESSAGES
     */
    public static final String MSG_MOBILE_NUMBER_FORMAT_ERR = "手機號碼格式不正確，請重新輸入";
    public static final String MSG_PASSWORD_FORMAT_ERR      = "密碼格式不正確，請重新輸入";
    public static final String MSG_AUTH_CODE_FORMAT_ERR     = "驗證碼格式不正確，請重新輸入";



    public static final String TAIPEI            = "臺北市";
    public static final String NEW_TAIPEI        = "新北市";
    public static final String TAOYUAN           = "桃園市";
    public static final String TAICHUNG          = "臺中市";
    public static final String TAINAN            = "臺南市";
    public static final String KAOHSIUNG         = "高雄市";
    public static final String KEELUNG           = "基隆市";
    public static final String HSINCHU           = "新竹市";
    public static final String HSINCHUCOUNTY     = "新竹縣";
    public static final String MIAOLI            = "苗栗縣";
    public static final String CHANGHUA          = "彰化縣";
    public static final String NANTOU            = "南投縣";
    public static final String YUNLIN            = "雲林縣";
    public static final String CHIAYI            = "嘉義市";
    public static final String CHIAYICOUNTY      = "嘉義縣";
    public static final String PINGTUNG          = "屏東縣";
    public static final String YILAN             = "宜蘭縣";
    public static final String HUALIEN           = "花蓮縣";
    public static final String TAITUNG           = "臺東縣";
    public static final String PENGHU            = "澎湖縣";
    public static final String KINMEN            = "金門縣";
    public static final String LIENCHIANG        = "連江縣";
    public static final String NANHAIZHUDAO      = "南海諸島";

    public static final String HAVE              = "有";

    public static String WEATHER_API (String CityName){
        return "https://api.openweathermap.org/data/2.5/weather?q="+CityName+",tw&APPID=df66879e8126dc8accebb8f77e631320&lang=zh_tw&units=metric";
    }
}
