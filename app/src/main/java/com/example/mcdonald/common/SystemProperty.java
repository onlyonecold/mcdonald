package com.example.mcdonald.common;

import android.util.Log;

public class SystemProperty {

    private static final String TAG = "SystemProperty";
    private static  final String URL_HOST_EXTERNAL = "http://192.168.50.227:8080/mcdonald";
    private static final String URL_HOST_APP_PATH = "/mcdonaldApi";

    public static String API_URL(String api){
        StringBuffer sb = new StringBuffer();
        sb.setLength(0);
        sb.append(URL_HOST_EXTERNAL).append(URL_HOST_APP_PATH).append(api);
        Log.i(TAG,"api url is "+sb.toString());
        return sb.toString();
    }
    public static final String AUTH_CODE_API = API_URL("/authCode");
    public static final String REGISTER_CODE_API = API_URL("/register");
    public static final String REGISTER_DETAIL_CODE_API = API_URL("/registerDetail");
    public static final String SING_IN_API = API_URL("/singIn");
    public static final String FORGET_PASSWORD = API_URL("/forgetPassword");

}
