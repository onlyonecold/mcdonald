package com.example.mcdonald;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.common.Constants;
import com.example.mcdonald.databinding.ActivityForgetPasswordBinding;
import com.example.mcdonald.helper.OKHttpHelper;
import com.example.mcdonald.util.AlertTool;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ForgetPasswordActivity extends AppCompatActivity {
    private static final String TAG = "ForgetPasswordActivity";
    ActivityForgetPasswordBinding binding;
    ImageButton imageBack;
    Button btnSubmit;
    EditText editMobile;
    TextView errMobile,errBirthday,textViewBirthday;

    String responseMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgetPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        findView();
        setListener();
        textViewBirthday.requestFocus();
        textViewBirthday.requestFocusFromTouch();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void findView(){
        editMobile = binding.editMobile;
        textViewBirthday = binding.textViewBirthday;
        errMobile = binding.errmobile;
        errBirthday = binding.errBirthday;
        btnSubmit = binding.btnSubmit;
        imageBack = binding.back;
    }

    private void setListener(){
        imageBack.setOnClickListener(back);
        textViewBirthday.setOnClickListener(calendar);
        btnSubmit.setOnClickListener(queryPassword);
    }

    //日期選擇器
    EditText.OnClickListener calendar = new EditText.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR,-20);
            new DatePickerDialog(ForgetPasswordActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    textViewBirthday.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
                }
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    Button.OnClickListener back = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setClass(ForgetPasswordActivity.this,SignInActivity.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener queryPassword = new Button.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            String mobile = editMobile.getText().toString().trim();
            String birthday = textViewBirthday.getText().toString().trim();

            if(mobile.trim().length() == 0)errMobile.setText(R.string.indeedWhite);
            else errMobile.setText(null);
            if(birthday.trim().length() == 0)errBirthday.setText(R.string.indeedWhite);
            else errBirthday.setText(null);
            if(mobile != "" && birthday != ""){
                if(!mobile.matches(Constants.MOBILE_PHONE_VERIFICATION)) AlertTool.getAlertDialog(ForgetPasswordActivity.this,getString(R.string.app_name),getString(R.string.mobileFormatErr)).show();
                else forgetPassword(mobile, birthday);
            }
        }
    };

    private void forgetPassword(String mobile, String birthday){
        Log.i(TAG,"mobile is "+mobile+" birthday is "+birthday);
        OKHttpHelper.forgetPassword(mobile, birthday, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "IOException:", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    try {
                        String responseStr = response.body().string();
                        Log.i(TAG,"responseStr is "+responseStr);
                        JSONObject jsonObject = new JSONObject(responseStr);
                        responseMessage =jsonObject.getString(Constants.JSON_MESSAGE);
                        final boolean success = jsonObject.getBoolean(Constants.JSON_SUCCESS);
                        Log.i(TAG,"responseMessage is "+responseMessage);
                        ForgetPasswordActivity.this.runOnUiThread(new Runnable( ) {
                            @Override
                            public void run() {
                                if(!success) AlertTool.getAlertDialog(ForgetPasswordActivity.this,getString(R.string.title),responseMessage).show();
                            }
                        });
                        if (jsonObject.getBoolean(Constants.JSON_SUCCESS)) {
                            Intent intent = new Intent();
                            intent.setClass(ForgetPasswordActivity.this,PasswordMessageActivity.class);
                            startActivity(intent);
                            finish();
                            Log.i(TAG,"success is true");
                        } else {
                            Log.i(TAG,"success is false");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }

            }
        });
    }
}
