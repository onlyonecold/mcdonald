package com.example.mcdonald.model;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.mcdonald.FavoriteActivity;
import com.example.mcdonald.GoogleMapActivity;
import com.example.mcdonald.HomeActivity;
import com.example.mcdonald.NewsActivity;
import com.example.mcdonald.R;
import com.example.mcdonald.databinding.NavigationDrawerBinding;
import com.google.android.material.navigation.NavigationView;

public class NavigetionBaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private final String TAG = "NavigetionBaseActivity";
    private  DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private  ActionBarDrawerToggle drawerToggle;
    private   NavigationView navigationView;
    private LinearLayout home_linear, favorite_linear, news_linear, point_card_linear, survey_linear,position_linear,help_linear,edit_linear,phone_linear;
    protected FrameLayout frames;
    private NavigationDrawerBinding binding;
    private static int currently;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = NavigationDrawerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(binding.getRoot());
        //切換畫面用
        frames = binding.frames;
        getLayoutInflater().inflate(layoutResID, frames);
        toolbar = findViewById(R.id.navigtion_toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = binding.navDrawerLayout;
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        findViewById();
        Log.i(TAG,"layoutResID is "+layoutResID );
        currently = layoutResID;
        setListener();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        DrawerLayout drawerLayout = findViewById(R.id.nav_drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
//
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.nav_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    public void findViewById(){
        home_linear = findViewById(R.id.home_linear);
        favorite_linear = findViewById(R.id.favorite_linear);
        news_linear = findViewById(R.id.news_linear);
        point_card_linear = findViewById(R.id.point_card_linear);
        survey_linear = findViewById(R.id.survey_linear);
        position_linear = findViewById(R.id.position_linear);
        help_linear = findViewById(R.id.help_linear);
        edit_linear = findViewById(R.id.edit_linear);
        phone_linear = findViewById(R.id.phone_linear);



    }

    public void setListener(){
        home_linear.setOnClickListener(home);
        favorite_linear.setOnClickListener(favorite);
        news_linear.setOnClickListener(news);
        point_card_linear.setOnClickListener(point);
        survey_linear.setOnClickListener(survey);
        position_linear.setOnClickListener(position);
        help_linear.setOnClickListener(help);
        edit_linear.setOnClickListener(edit);
        phone_linear.setOnClickListener(phone);
    }

    LinearLayout.OnClickListener home = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG,"home is "+R.layout.activity_home);
            if( currently != R.layout.activity_home) {
                Intent intent = new Intent( );
                intent.setClass(NavigetionBaseActivity.this, HomeActivity.class);
                startActivity(intent);
            }
            onBackPressed();
        }
    };

    LinearLayout.OnClickListener favorite = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            if( currently != R.layout.activity_favorite) {
                Intent intent = new Intent();
                intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
                startActivity(intent);
            }
            onBackPressed();
        }
    };

    LinearLayout.OnClickListener news = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            if( currently != R.layout.activity_news) {
                Intent intent = new Intent();
                intent.setClass(NavigetionBaseActivity.this, NewsActivity.class);
                startActivity(intent);
            }
            onBackPressed();
        }
    };

    LinearLayout.OnClickListener point = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };
    LinearLayout.OnClickListener survey = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };
    LinearLayout.OnClickListener position = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, GoogleMapActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };
    LinearLayout.OnClickListener  help = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };
    LinearLayout.OnClickListener edit = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };
    LinearLayout.OnClickListener phone = new LinearLayout.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(NavigetionBaseActivity.this, FavoriteActivity.class);
            startActivity(intent);
            onBackPressed();
        }
    };

}
