package com.example.mcdonald;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.common.Constants;
import com.example.mcdonald.databinding.ActivityUserDetailBinding;
import com.example.mcdonald.util.AlertTool;

import java.util.Calendar;
import java.util.regex.Pattern;

public class UserDetailActivity extends AppCompatActivity {
    private final static String TAG = "UserDetailActivity";
    private ActivityUserDetailBinding binding;
    private EditText detailSurname, detailName, detailAddress;
    private TextView derailMobile, detailGender, detailEmail,detailBirthday ,detailCity, detailRegion, detailPostalCode, detailMarital, detailChild,
            detailYoungestAge, errSurname, errName, errGender, errBirthday, errEmail, errCity;
    private LinearLayout layoutYoungestAge;
    private CheckBox checkBox;
    private Button btnNext;
    private boolean receive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        findView();
        setListener();
        detailBirthday.requestFocus();
        detailBirthday.requestFocusFromTouch();
        Bundle bundle = this.getIntent().getExtras();
        String mobile = bundle.getString("mobile");
        if(mobile !="")derailMobile.setText(mobile);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }



    private void findView(){
        derailMobile = findViewById(R.id.derailMobile);
        errSurname = findViewById(R.id.errSurname);
        errName = findViewById(R.id.errName); ;
        errGender = findViewById(R.id.errGender); ;
        errBirthday = findViewById(R.id.errBirthday); ;
        errEmail = findViewById(R.id.errEmail); ;
        errCity = findViewById(R.id.errCity); ;
        detailSurname = findViewById(R.id.detailsurname);
        detailName = findViewById(R.id.detailName);
        detailBirthday = findViewById(R.id.detailBirthday);
        detailAddress = findViewById(R.id.detailAddress);
        detailGender = findViewById(R.id.detailGender);
        detailEmail = findViewById(R.id.detailEmail);
        detailCity = findViewById(R.id.detailCity);
        detailRegion = findViewById(R.id.detailRegion);
        detailPostalCode = findViewById(R.id.detailPostalCode);
        detailMarital = findViewById(R.id.detailMarital);
        detailChild = findViewById(R.id.detailChild);
        detailYoungestAge = findViewById(R.id.detailYoungestAge);
        layoutYoungestAge = findViewById(R.id.layoutYoungestAge);
        checkBox = findViewById(R.id.checkBox);
        btnNext = findViewById(R.id.btnNext);
    }
    private void setListener() {
        btnNext.setOnClickListener(next);
        checkBox.setOnCheckedChangeListener(isYes);
        detailGender.setOnClickListener(genderDialog);
        detailCity.setOnClickListener(cityDialog);
        detailRegion.setOnClickListener(regionDialog);
        detailMarital.setOnClickListener(maritalStatusDialog);
        detailChild.setOnClickListener(childDialog);
        detailYoungestAge.setOnClickListener(ageDialog);
        detailBirthday.setOnClickListener(calendar);
    }

    //日期選擇器
    EditText.OnClickListener calendar = new EditText.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR,-20);
            new DatePickerDialog(UserDetailActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    detailBirthday.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
                }
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    //性別選擇
    TextView.OnClickListener genderDialog = new TextView.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            AlertTool.radioDialog(UserDetailActivity.this,R.string.gender_title, R.array.gender,detailGender).show();
        }
    };
    //縣市選擇
    TextView.OnClickListener cityDialog = new TextView.OnClickListener( ) {

        @Override
        public void onClick(View v) {
            AlertTool.cityDialog(UserDetailActivity.this,R.string.city_title, R.array.city,detailCity,detailRegion,detailPostalCode).show();
        }



    };
    //區域選擇
    TextView.OnClickListener regionDialog = new TextView.OnClickListener() {
        @Override
        public void onClick(View v) {
            regionSelect(detailCity.getText().toString());
        }
    };
    //婚姻選擇
    TextView.OnClickListener maritalStatusDialog = new TextView.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            AlertTool.radioDialog(UserDetailActivity.this,R.string.marital_title, R.array.marital,detailMarital).show();
        }
    };

    //小孩選擇
    TextView.OnClickListener childDialog = new TextView.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            AlertTool.childDialog(UserDetailActivity.this,R.string.child_title, R.array.child,detailChild, layoutYoungestAge).show();
        }
    };
    //最幼小孩年次
    TextView.OnClickListener ageDialog = new TextView.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            AlertDialog dialog =  AlertTool.yearDialog(UserDetailActivity.this,R.string.age_title,detailYoungestAge);
            dialog.show();
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.width = 1000;
            params.height = 1000;
            dialog.getWindow().setAttributes(params);
        }
    };

    //訂閱選擇
    CheckBox.OnCheckedChangeListener isYes = new CheckBox.OnCheckedChangeListener( ) {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) receive = true;
            else receive = false;
        }
    };

    private void regionSelect (String city) {
        int array = 0;
        int postalCode = 0;
        if(city.trim() != ""){
            switch (city){
                case Constants.TAIPEI:
                    array = R.array.taipei;
                    postalCode = R.array.taipei_postal_code;
                    break;
                case Constants.NEW_TAIPEI:
                    array = R.array.new_taipei;
                    postalCode = R.array.new_taipei_postal_code;
                    break;
                case Constants.TAOYUAN:
                    array =R.array.taoyuan;
                    postalCode = R.array.taoyuan_postal_code;;
                    break;
                case Constants.TAICHUNG:
                    array = R.array.taichung;
                    postalCode = R.array.taichung_postal_code;
                    break;
                case Constants.TAINAN:
                    array = R.array.tainan;
                    postalCode = R.array.tainan_postal_code;
                    break;
                case Constants.KAOHSIUNG:
                    array = R.array.kaohsiung;
                    postalCode = R.array.kaohsiung_postal_code;
                    break;
               //case Constants.KEELUNG:
               //    break;
               //case Constants.HSINCHU:
               //    break;
               //case Constants.HSINCHUCOUNTY:
               //    break;
               //case Constants.MIAOLI:
               //    break;
               //case Constants.CHANGHUA:
               //    break;
               //case Constants.NANTOU:
               //    break;
               //case Constants.YUNLIN:
               //    break;
               //case Constants.CHIAYI:
               //    break;
               //case Constants.CHIAYICOUNTY:
               //    break;
               //case Constants.PINGTUNG:
               //    break;
               //case Constants.YILAN:
               //    break;
               //case Constants.HUALIEN:
               //    break;
               //case Constants.TAITUNG:
               //    break;
               //case Constants.PENGHU:
               //    break;
               //case Constants.KINMEN:
               //    break;
               //case Constants.LIENCHIANG:
               //    break;
               //case Constants.NANHAIZHUDAO:
               //    break;

            }
            if( array != 0)AlertTool.regionAndPostalCodeDialog(UserDetailActivity.this, R.string.region_title, array,postalCode, detailRegion,detailPostalCode).show( );
        }
    }

    Button.OnClickListener next = new Button.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            String mobile = derailMobile.getText().toString().trim();
            String surname = detailSurname.getText().toString().trim();
            String name = detailName.getText().toString().trim();
            String birthday = detailBirthday.getText().toString().trim();
            String address = detailAddress.getText().toString().trim();
            String gender = detailGender.getText().toString().trim();
            String email = detailEmail.getText().toString().trim();
            String city = detailCity.getText().toString().trim();
            String region =  detailRegion.getText().toString().trim();
            String postalCode = detailPostalCode.getText().toString().trim();
            String marital = detailMarital.getText().toString().trim();
            String child =  detailChild.getText().toString().trim();
            String youngestAge = detailYoungestAge.getText().toString().trim();
            if(surname.length() == 0)errSurname.setText(R.string.indeedWhite);
            else errSurname.setText(null);
            if(name.length() == 0)errName.setText(R.string.indeedWhite);
            else errName.setText(null);
            if(birthday.length() == 0)errBirthday.setText(R.string.indeedWhite);
            else errBirthday.setText(null);
            if(gender.length() == 0)errGender.setText(R.string.indeedWhite);
            else errGender.setText(null);
            if(email.length() == 0)errEmail.setText(R.string.indeedWhite);
            else errEmail.setText(null);
            if(city.length() == 0)errCity.setText(R.string.indeedWhite);
            else errCity.setText(null);
            if(surname.length() != 0 && name.length() != 0 && birthday.length() != 0
                    && gender.length() != 0 && email.length() != 0 && city.length() != 0){
                Pattern pattern = Pattern.compile(Constants.EMAIL_VERIFICATION);
                if(!pattern.matcher(email).find()){
                    AlertTool.getAlertDialog(UserDetailActivity.this,getString(R.string.app_name),getString(R.string.emailFormatErr)).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(UserDetailActivity.this,CompleteRegisterActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("mobile",mobile);
                    bundle.putString("surname",surname);
                    bundle.putString("name",name);
                    bundle.putString("birthday",birthday);
                    bundle.putString("address",address);
                    bundle.putString("gender",gender);
                    bundle.putString("email",email);
                    bundle.putString("city",city);
                    bundle.putString("region",region);
                    bundle.putString("postalCode",postalCode);
                    bundle.putString("marital",marital);
                    bundle.putString("child",child);
                    bundle.putString("youngestAge",youngestAge);
                    bundle.putString("receive",String.valueOf(receive));
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();


                }

            }

        }
    };



}
