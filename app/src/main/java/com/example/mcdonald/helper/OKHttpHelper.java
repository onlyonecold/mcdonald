package com.example.mcdonald.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.mcdonald.bean.UserDetail;
import com.example.mcdonald.common.Constants;
import com.example.mcdonald.common.SystemProperty;

import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class OKHttpHelper {
    private static final String TAG = "OKHttpHelper";

    /**
     * 確認是否有開啟網路
     *
     * @param context, activity context
     * @return boolean, 是否有網路
     */

    public static boolean hasNetWork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            return (networkInfo != null && networkInfo.isConnected());
        }
        return false;
    }

    private static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(Constants.WAIT_5_MINS, TimeUnit.SECONDS)
                .writeTimeout(Constants.WAIT_5_MINS, TimeUnit.SECONDS)
                .readTimeout(Constants.WAIT_5_MINS, TimeUnit.SECONDS)
                .build();
        return client;
    }

    public static void authCode(String mobile, Callback mCallback) {
        OkHttpClient client = getClient();
        MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBody.addFormDataPart(Constants.PARAMETER_MOBILE, mobile);
        Request request = new Request.Builder().url(SystemProperty.AUTH_CODE_API).post(multipartBody.build()).build();
        client.newCall(request).enqueue(mCallback);
    }

    public static void register (String mobile, String password, Callback mCallback) {
        OkHttpClient client = getClient();
        MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBody.addFormDataPart(Constants.PARAMETER_MOBILE, mobile);
        multipartBody.addFormDataPart(Constants.PASSWORD, password);
        Request request = new Request.Builder().url(SystemProperty.REGISTER_CODE_API).post(multipartBody.build()).build();
        client.newCall(request).enqueue(mCallback);
    }

    public static void registerDetail (String mobile, UserDetail userDetail, Callback mCallback) {
        OkHttpClient client = getClient();
        MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBody.addFormDataPart(Constants.PARAMETER_MOBILE, mobile);
        multipartBody.addFormDataPart(Constants.JSON_SURNAME, userDetail.getName());
        multipartBody.addFormDataPart(Constants.JSON_NAME, userDetail.getName());
        multipartBody.addFormDataPart(Constants.JSON_GENDER, userDetail.getGender());
        multipartBody.addFormDataPart(Constants.JSON_BIRTHDAY, userDetail.getBirthday());
        multipartBody.addFormDataPart(Constants.JSON_EMAIL, userDetail.getEmail());
        multipartBody.addFormDataPart(Constants.JSON_CITY, userDetail.getCity());
        if(userDetail.getRegion() != null)multipartBody.addFormDataPart(Constants.JSON_REGION, userDetail.getRegion());
        if(userDetail.getPostalCode() != null)multipartBody.addFormDataPart(Constants.JSON_POSTAL_CODE, userDetail.getPostalCode());
        if(userDetail.getAddress() != null)multipartBody.addFormDataPart(Constants.JSON_ADDRESS, userDetail.getAddress());
        if(userDetail.getMaritalStatus() != null)multipartBody.addFormDataPart(Constants.JSON_MARITAL_STATUS, userDetail.getMaritalStatus());
        if(userDetail.getChild() != null)multipartBody.addFormDataPart(Constants.JSON_CHILD, userDetail.getChild());
        if(userDetail.getYoungestAge() != null)multipartBody.addFormDataPart(Constants.JSON_YOUNGEST_AGE,  userDetail.getYoungestAge());
        if(userDetail.getReceive() != null)multipartBody.addFormDataPart(Constants.JSON_RECEIVE, userDetail.getReceive());
        if(userDetail.getFacebookId() != null)multipartBody.addFormDataPart(Constants.JSON_FACEBOOK_ID, userDetail.getFacebookId());
        if(userDetail.getGoogleId() != null)multipartBody.addFormDataPart(Constants.JSON_GOOGLE_ID, userDetail.getGoogleId());
        Request request = new Request.Builder().url(SystemProperty.REGISTER_DETAIL_CODE_API).post(multipartBody.build()).build();
        client.newCall(request).enqueue(mCallback);
    }

    public static void singIn(String mobile,String password, Callback mCallback) {
        OkHttpClient client = getClient();
        MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBody.addFormDataPart(Constants.PARAMETER_MOBILE, mobile);
        multipartBody.addFormDataPart(Constants.PASSWORD, password);
        Request request = new Request.Builder().url(SystemProperty.SING_IN_API).post(multipartBody.build()).build();
        client.newCall(request).enqueue(mCallback);
    }

    public static void forgetPassword(String mobile,String birthday, Callback mCallback) {
        OkHttpClient client = getClient();
        MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        multipartBody.addFormDataPart(Constants.PARAMETER_MOBILE, mobile);
        multipartBody.addFormDataPart(Constants.JSON_BIRTHDAY, birthday);
        Request request = new Request.Builder().url(SystemProperty.FORGET_PASSWORD).post(multipartBody.build()).build();
        client.newCall(request).enqueue(mCallback);
    }

}
