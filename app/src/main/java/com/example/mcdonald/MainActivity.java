package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btn_main_newUser;
    private Button btn_main_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        setListener();
    }

    private void findView(){
        btn_main_newUser=findViewById(R.id.btn_main_newuser);
        btn_main_user=findViewById(R.id.btn_main_user);

    }
    private void setListener(){
        btn_main_newUser.setOnClickListener(newUser);
        btn_main_user.setOnClickListener(signIn);
    }

    Button.OnClickListener newUser= new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this, NewUserActivity.class));



        }
    };
    Button.OnClickListener signIn= new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
            finish();
        }
    };
}
