package com.example.mcdonald.service;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FireBaseService extends FirebaseMessagingService {
    private final static String TAG = "FireBaseService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getNotification() != null) {
            Log.i(TAG,"title "+remoteMessage.getNotification().getTitle());
            Log.i(TAG,"body "+remoteMessage.getNotification().getBody());
        }


    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i(TAG,"token "+s);
    }
}
