package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class LogoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(1000);

                }catch (Exception e){
                    e.printStackTrace();

                }
                finally {
                    Intent intent = new Intent();
                    intent.setClass(LogoActivity.this,MainActivity.class);
                    startActivity(intent);
                    LogoActivity.this.finish();

                }

            }
        }).start();
        ImageView iv_logo_circle=(ImageView)findViewById(R.id.iv_logo_circle);

        //旋轉
        //RotateAnimation(開始角度，結束角度，X軸選轉方式，X軸偏移量，Ｙ軸選轉方式，Ｙ軸偏移量)
        Animation rotateAnimation = new RotateAnimation(0,360, Animation.RESTART,0.5f, Animation.RESTART,0.5f);
        //如果fillAfter為true，則此動畫執行的轉換將在完成後保留。
        rotateAnimation.setFillAfter(true);
        //設定選轉持續時間
        rotateAnimation.setDuration(1000);
        //設置此動畫的加速度曲線。
        rotateAnimation.setInterpolator(new LinearInterpolator());
        iv_logo_circle.startAnimation(rotateAnimation);


    }
}
