package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.bean.UserDetail;
import com.example.mcdonald.common.Constants;
import com.example.mcdonald.databinding.ActivityCompleteRegisterBinding;
import com.example.mcdonald.helper.OKHttpHelper;
import com.example.mcdonald.util.AlertTool;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class CompleteRegisterActivity extends AppCompatActivity {
    private String responseMessage;
    private static final String TAG = "RegisterActivity";
    ActivityCompleteRegisterBinding binding;
    TextView btnRegister;
    Button btnFacebook, btnGoogle;
    Bundle bundle;
    private String googleId, facebookId;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleSignInClient googleSignInClient;
    private LoginManager loginManager;
    private CallbackManager callbackManager;
    private int RC_SIGN_IN = 0; // 設定變數值

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCompleteRegisterBinding.inflate(getLayoutInflater( ));
        setContentView(binding.getRoot( ));
        bundle = this.getIntent( ).getExtras( );
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail( ).build( );
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        loginManager = LoginManager.getInstance( );
        callbackManager = CallbackManager.Factory.create( );
        findView( );
        setListener( );

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void findView() {
        btnRegister = binding.btnRegister;
        btnFacebook = binding.btnFacebook;
        btnGoogle = binding.btnGoogle;
    }

    private void setListener() {
        btnRegister.setOnClickListener(register);
        btnFacebook.setOnClickListener(loginFaceBook);
        btnGoogle.setOnClickListener(googleLogin);
    }

    TextView.OnClickListener register = new TextView.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            String mobile = bundle.getString("mobile");
            String surname = bundle.getString("surname");
            String name = bundle.getString("name");
            String birthday = bundle.getString("birthday");
            String address = bundle.getString("address");
            String gender = bundle.getString("gender");
            String email = bundle.getString("email");
            String city = bundle.getString("city");
            String region = bundle.getString("region");
            String postalCode = bundle.getString("postalCode");
            String marital = bundle.getString("marital");
            String child = bundle.getString("child");
            String youngestAge = bundle.getString("youngestAge");
            String receive = bundle.getString("receive");
            UserDetail userDetail = new UserDetail( );
            userDetail.setSurname(surname);
            userDetail.setName(name);
            userDetail.setBirthday(birthday);
            userDetail.setAddress(address);
            userDetail.setGender(gender);
            userDetail.setEmail(email);
            userDetail.setCity(city);
            userDetail.setRegion(region);
            userDetail.setPostalCode(postalCode);
            userDetail.setMaritalStatus(marital);
            userDetail.setChild(child);
            userDetail.setYoungestAge(youngestAge);
            userDetail.setReceive(receive);
            userDetail.setFacebookId(facebookId);
            userDetail.setGoogleId(googleId);
            registerDetail(mobile, userDetail);

        }
    };

    Button.OnClickListener googleLogin = new Button.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            signIn( );
        }
    };

    Button.OnClickListener loginFaceBook = new Button.OnClickListener( ) {
        @Override
        public void onClick(View view) {
            initFB( );
//            FaceBookSingIn faceBookSingIn =  new FaceBookSingIn(CompleteRegisterActivity.this,loginManager,callbackManager);
//            faceBookSingIn.initFB();

        }
    };

    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent( );
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onSuccess:onActivityResult!");
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (acct != null) {  //登入的帳號不為空
                String personName = acct.getDisplayName( );
                String personGivenName = acct.getGivenName( );
                String personFamilyName = acct.getFamilyName( );
                String personEmail = acct.getEmail( );
                String personId = acct.getId( );
                googleId = personId;
                Log.i(TAG, "personName is " + personName + " personGivenName is " + personGivenName + " personFamilyName is " + personFamilyName + " personEmail is " + personEmail + " personId is " + personId);
            }
        } catch (ApiException e) {
            Log.e(TAG, "handleSignInResult is error" + e);
        }
    }

    private void initFB() {
        // init facebook

        // init LoginManager & CallbackManager
        loginManager = LoginManager.getInstance( );
        callbackManager = CallbackManager.Factory.create( );
        AccessToken accessToken = AccessToken.getCurrentAccessToken( );  //FB condition accessToken
        loginFB( );
    }

    private void loginFB() {
        // 設定FB login的顯示方式 ; 預設是：NATIVE_WITH_FALLBACK
        /**
         * 1. NATIVE_WITH_FALLBACK
         * 2. NATIVE_ONLY
         * 3. KATANA_ONLY
         * 4. WEB_ONLY
         * 5. WEB_VIEW_ONLY
         * 6. DEVICE_AUTH
         */
        loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        // 設定要跟用戶取得的權限，以下3個是基本可以取得，不需要經過FB的審核
        List<String> permissions = new ArrayList<>( );
        permissions.add("public_profile");
        permissions.add("email");

        // 設定要讀取的權限
        loginManager.logInWithReadPermissions(this, permissions);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>( ) {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // 登入成功
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken( ), new GraphRequest.GraphJSONObjectCallback( ) {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            if (response.getConnection( ).getResponseCode( ) == 200) {
                                long id = object.getLong("id");
                                facebookId = String.valueOf(id);
                                String name = object.getString("name");
                                String email = object.getString("email");
                                Log.d(TAG, "Facebook id:" + id);
                                Log.d(TAG, "Facebook name:" + name);
                                Log.d(TAG, "Facebook email:" + email);

                            }
                        } catch (IOException e) {
                            e.printStackTrace( );
                        } catch (JSONException e) {
                            e.printStackTrace( );
                        }
                    }
                });

                Bundle parameters = new Bundle( );
                parameters.putString("fields", "id,name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync( );
            }

            @Override
            public void onCancel() {
                // 用戶取消
                Log.d(TAG, "Facebook onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                // 登入失敗
                Log.d(TAG, "Facebook onError:" + error.toString( ));
            }
        });
    }


    private void registerDetail(String mobile, UserDetail userDetail) {
        OKHttpHelper.registerDetail(mobile, userDetail, new Callback( ) {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "IOException:", e);
            }

            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful( )) {
                    try {
                        String responseStr = response.body( ).string( );
                        Log.i(TAG, "responseStr is " + responseStr);
                        JSONObject jsonObject = new JSONObject(responseStr);
                        responseMessage = jsonObject.getString(Constants.JSON_MESSAGE);
                        final boolean success = jsonObject.getBoolean(Constants.JSON_SUCCESS);
                        Log.i(TAG, "responseMessage is " + responseMessage);
                        CompleteRegisterActivity.this.runOnUiThread(new Runnable( ) {
                            @Override
                            public void run() {
                                if (!success)
                                    AlertTool.getAlertDialog(CompleteRegisterActivity.this, getString(R.string.title), responseMessage).show( );
                            }
                        });
                        if (jsonObject.getBoolean(Constants.JSON_SUCCESS)) {
                            Intent intent = new Intent( );
                            intent.setClass(CompleteRegisterActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish( );
                            Log.i(TAG, "success is true");
                        } else {
                            Log.i(TAG, "success is false");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        });
    }
}
