package com.example.mcdonald.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mcdonald.R;
import com.example.mcdonald.bean.Coupon;

import java.util.List;

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.ViewHolder> {

    private Context mContext;
    private List<Coupon> couponList;

    public CouponAdapter(List<Coupon> couponList){
        this.couponList = couponList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View couponView;
        ImageView couponImage;
        TextView couponName;

        public ViewHolder(View view) {
            super(view);
            couponView = view;
            couponImage =view.findViewById(R.id.coupon_image);
            couponName =view.findViewById(R.id.coupon_name);

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coupon_model, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.couponView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Coupon coupon = couponList.get(position);
                Toast.makeText(view.getContext(), "你點擊了View"+ coupon.getCouponName(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.couponImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Coupon coupon = couponList.get(position);

                Uri uri = Uri.parse("https://tw.news.yahoo.com/");
                Intent changeIntent = new Intent(Intent.ACTION_VIEW,uri);
                mContext.startActivity(changeIntent);

            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Coupon coupon = couponList.get(position);
        holder.couponImage.setImageResource(coupon.getCouponImage());
        holder.couponName.setText(coupon.getCouponName());
    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }


}
