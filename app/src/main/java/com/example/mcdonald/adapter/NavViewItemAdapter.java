package com.example.mcdonald.adapter;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mcdonald.R;

import java.util.List;
import java.util.Map;

public class NavViewItemAdapter extends BaseAdapter {
    protected Context context;
    protected List<Map<String,Object>> itemList;
    protected TextView textView;
    protected LinearLayout nav_relativ_layout;
    public NavViewItemAdapter(Context context,List<Map<String,Object>> list){
        this.context = context;
        this.itemList =  list;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(this.context, R.layout.nav_view_items,null);
        Map<String,Object> map = itemList.get(position);
        Integer id =(Integer)map.get("id");
        String str = (String) map.get("str");
         ((ImageView)view.findViewById(R.id.nav_icon_image)).setImageResource(id);
        nav_relativ_layout = view.findViewById(R.id.nav_relativ_layout);
        textView = view.findViewById(R.id.nav_icon_text);
        textView.setText(str);
        if(position <= 4) {
            nav_relativ_layout.setBackgroundColor(context.getResources().getColor(R.color.red));
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }


        return view;
    }

}
