package com.example.mcdonald.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.mcdonald.util.AlertTool;

import java.util.List;

//PagerAdapter物件
public class ViewAdapter extends PagerAdapter {
    private final static String TAG = "ViewAdapter";
    //頁面計數計
    public static final int count = 500; //計數器
    private List<View> viewList;
    //建構子
    public ViewAdapter(List<View> itemList) {
        //將畫面加入建構子內
        this.viewList = itemList;
    }
    //*2019.6.20
    // 解决：如果getCount 返回值為Integer.MAX_VALUE 的话，那么在setCurrentItem的时候会ANR(除了在onCreate 调用之外)
    @Override
    public int getCount() {
        //目前圖片張數*500
        return viewList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return view == object;
    }
    //從當前container中删除指定位置（position的View;
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }

    //nstantiateItem()：做了两件事，第一：将當前視圖添加到container中，第二：返回當前View
    @Override
    public Object instantiateItem( ViewGroup container, int position) {
        View view = viewList.get(position);
        final ViewGroup vg = container;
        Log.i(TAG,"position is "+position);
        if(position == 0){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertTool.getAlertDialog(vg.getContext(),"麥當勞報報","離下次天氣提醒尚餘\n10小時30分").show();
                }
            });

        }
        container.addView(viewList.get(position));
        return viewList.get(position);

    }

}
