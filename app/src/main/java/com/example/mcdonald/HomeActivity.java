package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.example.mcdonald.adapter.ViewAdapter;
import com.example.mcdonald.model.NavigetionBaseActivity;

import java.util.ArrayList;
import java.util.List;

import static android.widget.ImageView.ScaleType.FIT_START;


public class HomeActivity extends NavigetionBaseActivity {
    private final String TAG = "HomeActivity";
    ImageView[] tips = null;
    ViewGroup.MarginLayoutParams pointEdit;
    ViewGroup viewGroup;
    ImageView imageView;
    ImageView.ScaleType scaleType;
    Button btnCoupon;
    private ViewAdapter viewAdapter;
    private Handler mHandler = new Handler();
    private long delayedTime = 5000;
    private ViewPager viewPager;
    private List<View> itemList = new ArrayList<>();
    private int currentPosition;
    private boolean autoPlay = true;
    private LinearLayout page01;
    private final Runnable mLoopRunnable = new Runnable() {
        @Override
        public void run() {
            if (autoPlay) {
                //currentPosition ＝ 當前位置
                currentPosition = viewPager.getCurrentItem();
//                currentPosition = (currentPosition + 1) % imageAdapter.getCount();
                currentPosition++;
                if (currentPosition == viewAdapter.getCount()) {
                    currentPosition = 0;   //为了循环所以变为0
                    viewPager.setCurrentItem(currentPosition, false);
                    //每次切換頁面暫停秒數
                    mHandler.postDelayed(this, delayedTime);
                    tips[viewAdapter.getCount()-1].setBackgroundResource(R.mipmap.black_point);
                    finish(viewPager);

                } else {
                    viewPager.setCurrentItem(currentPosition);
                    mHandler.postDelayed(this, delayedTime);
                }
            } else {
                mHandler.postDelayed(this, delayedTime);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findView();
        initData();
        setListeners();
        mHandler.postDelayed(mLoopRunnable, delayedTime);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //开始轮播

    }

    @Override
    protected void onPause() {
        super.onPause();
        //停止轮播
        mHandler.removeCallbacks(mLoopRunnable);
    }

    private void findView() {
        viewPager = findViewById(R.id.viewPager);
        viewGroup = findViewById(R.id.viewGroup);
        btnCoupon = findViewById(R.id.btnCoupon);
    }

    private void setListeners(){

        btnCoupon.setOnClickListener(coupon);
    }

    Button.OnClickListener coupon = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(HomeActivity.this,CouponActivity.class);
            startActivity(intent);
            finish();

        }
    };

    private void initData() {
        View weather_page = getLayoutInflater().inflate(R.layout.weather_page,null);
        View set_weather = getLayoutInflater().inflate(R.layout.set_weather_page,null);
        itemList.add(weather_page);
        itemList.add(set_weather);

        //取得導覽列
        Log.i(TAG,"itemList length is "+itemList.size());
        if(itemList.size()!=0){
            setPoint(itemList);
            //將增加好的圖片陣列放入ImageAdapter內
            viewAdapter = new ViewAdapter(itemList);
            //將設定好的imageAdapter置入
            viewPager.setAdapter(viewAdapter);
            currentPosition = getStartItem();
            //view_Pager.setCurrentItem(currentPosition);
            viewPager.addOnPageChangeListener(new GuidePageChangeListener());
        }
    }

    private void setPoint(List<View> viewList){
        //取得導覽列長度
        tips = new ImageView[viewList.size()];
       // Log.i(TAG,"viewList.size() is "+viewList.size());
        for (int page = 0; page < viewList.size(); page++) {
            //透過迴圈建立導覽列
            imageView = new ImageView(HomeActivity.this);
            pointEdit = new ViewGroup.MarginLayoutParams(30, 30);
            pointEdit.leftMargin = 50;
            scaleType = FIT_START;
            //設定導覽圖像寬及高
            imageView.setLayoutParams(pointEdit);
            imageView.setScaleType(scaleType);
            //將導覽圖像加入陣列內
            tips[page] = imageView;

            //預設第一張圖顯示為選中狀態
            if (page == 0) {
                tips[page].setBackgroundResource(R.mipmap.red_point);
            } else {
                tips[page].setBackgroundResource(R.mipmap.black_point);
            }
            //將每個原點加入view_group_lin_lay內
            viewGroup.addView(tips[page]);
        }
    }

    private int getStartItem() {
        //判斷當前陣列長度是否為零
        if(getRealCount() == 0){
            return 0;
        }
        //取得首頁
        int currentItem = getRealCount() * viewAdapter.count /2;
        if(currentItem % getRealCount() == 0 ){
            return currentItem;
        }
        // 直到找到从0开始的位置
        while (currentItem % getRealCount() != 0){
            currentItem++;
        }
        return currentItem;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private int getRealCount() {
        //取得目前圖片陣列長度
        return  itemList== null ? 0 : itemList.size();
    }

    //滑動到最後一個刷新頁面
    public void finish(View view) {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
        finish();
    }

    public class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        //切換view時，下方圓點的變化。
        public void onPageSelected(int position) {
            //position = 目前位置
            tips[position].setBackgroundResource(R.mipmap.red_point);
            //這個圖片就是選中的view的圓點
            for (int i = 0; i <= position; i++) {
                if (position != i) {
                    //這個圖片是未選中view的圓點
                    tips[i].setBackgroundResource(R.mipmap.black_point);
                }
            }
            //預防往回滑動時，原本那個顏色沒變
            try{
                tips[position+1].setBackgroundResource(R.mipmap.black_point);
            }catch (Exception e){

            }
        }
    }
}
