package com.example.mcdonald;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mcdonald.adapter.CouponAdapter;
import com.example.mcdonald.bean.Coupon;
import com.example.mcdonald.model.NavigetionBaseActivity;

import java.util.ArrayList;
import java.util.List;

public class CouponActivity extends NavigetionBaseActivity {

    private RecyclerView coupon_recyclerView;
    private List<Coupon> couponList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        findView();
        initCouponList();
        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        coupon_recyclerView.setLayoutManager(layoutManager);
        CouponAdapter adapter = new CouponAdapter(couponList);
        coupon_recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void findView(){
        coupon_recyclerView = findViewById(R.id.coupon_recyclerView);
    }

    private void initCouponList() {
        for(int i = 0; i < 2; i++) {
            Coupon coupon = new Coupon( R.drawable.weather_icon_01d,"Apple");
            couponList.add(coupon);
            Coupon coupon2 = new Coupon( R.drawable.weather_icon_01n,"Apple");
            couponList.add(coupon2);
        }
    }
}
