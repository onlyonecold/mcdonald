package com.example.mcdonald;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.common.Constants;
import com.example.mcdonald.databinding.ActivityNewUserBinding;
import com.example.mcdonald.helper.OKHttpHelper;
import com.example.mcdonald.util.AlertTool;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class NewUserActivity extends AppCompatActivity {
    private ImageButton backBtn;
    private Button btnActhCode,btnRegister;
    private ActivityNewUserBinding binding;
    private EditText editPhone, editPwd, editChickPwd,editAuthCode;
    private TextView errPhone, errPwd, errChickPwd, errAuthCode;
    private static final String TAG = "NewUserActivity";
    private String responseMessage ;
    private boolean click = true ;
    Boolean hasMessageCode = false;// 做傳送成功或失敗判斷
    private String authCode;//ID碼
    private FirebaseAuth mAuth;//Firebase驗證元件


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  ActivityNewUserBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        findView();
        setListener();
        mAuth = FirebaseAuth.getInstance( );
        //簡訊認證碼,美國-英文:en-US,繁體中文格式ZH-TW,zh-Tw,簡體中文代碼ZH-CN,zh-CN
        mAuth.setLanguageCode("zh-Tw");

    }

    @Override
    protected void onResume() {

        super.onResume( );
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void findView(){
        backBtn = binding.backBtn;
        btnActhCode = binding.btnVerification;
        btnRegister = binding.btnRegister;
        editPhone = binding.editPhone;
        editPwd = binding.editPwd;
        editChickPwd = binding.editChickPwd;
        editAuthCode = binding.editVerification;
        errPhone = binding.errPhone;
        errPwd = binding.errPwd;
        errChickPwd = binding.errChickPwd;
        errAuthCode = binding.errVerification;

    }

    public void setListener(){
        editPhone.setOnClickListener(remind);
        backBtn.setOnClickListener(goBack);
        btnActhCode.setOnClickListener(sendAuthCode);
        btnRegister.setOnClickListener(chickAuthCodeChickAndRegister);
    }

    Button.OnClickListener goBack = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    EditText.OnClickListener remind = new EditText.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            if(click)AlertTool.getAlertDialog(NewUserActivity.this,getString(R.string.title),getString(R.string.remind)).show();
            editPhone.setFocusable(true);
            editPhone.setFocusableInTouchMode(true);
            editPhone.requestFocus();
            editPhone.requestFocusFromTouch();
            InputMethodManager inputMethodManager = (InputMethodManager) editPhone.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(editPhone,0);
            click = false;
        }
    };

    Button.OnClickListener sendAuthCode = new Button.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            String mobile = editPhone.getText( ).toString( ).trim( );
            errPhone.setText(null);
            if (mobile.length() == 0) {
                errPhone.setText(R.string.indeedWhite);
            } else {
                if (mobile.matches(Constants.MOBILE_PHONE_VERIFICATION)) {
                    errPhone.setText(null);
                    authCode(mobile);
                } else {
                    AlertTool.getAlertDialog(NewUserActivity.this, getString(R.string.title), Constants.MSG_MOBILE_NUMBER_FORMAT_ERR).show( );
                }
            }
        }
    } ;

    Button.OnClickListener chickAuthCodeChickAndRegister = new Button.OnClickListener( ) {
        @Override
        public void onClick(View v) {
            errPhone.setText(null);
            errPwd.setText(null);
            errChickPwd.setText(null);
            String mobile = editPhone.getText().toString().trim();
            String password = editPwd.getText().toString().trim();
            String chickPassword = editChickPwd.getText().toString().trim();
            String authCode = editAuthCode.getText().toString().trim();
            if(mobile.length() == 0) errPhone.setText(R.string.indeedWhite);
            if(password.length() == 0) errPwd.setText(R.string.indeedWhite);
            if(chickPassword.length() == 0)errChickPwd.setText(R.string.indeedWhite);
            if(authCode.length() == 0 )errAuthCode.setText(R.string.indeedWhite);
            if(mobile.length()!=0 && password.length()!=0 && chickPassword.length()!=0 && authCode.length()!=0 ){
                if(!mobile.matches(Constants.MOBILE_PHONE_VERIFICATION)){
                    AlertTool.getAlertDialog(NewUserActivity.this,getString(R.string.title),Constants.MSG_MOBILE_NUMBER_FORMAT_ERR).show();
                }else {
                    if (!password.equals(chickPassword)) {
                        AlertTool.getAlertDialog(NewUserActivity.this, getString(R.string.title), Constants.MSG_PASSWORD_FORMAT_ERR).show();
                    } else {
                        if (!password.matches(Constants.PASSWORD_VERIFICATION)) {
                            AlertTool.getAlertDialog(NewUserActivity.this, getString(R.string.title), Constants.MSG_PASSWORD_FORMAT_ERR).show();
                        } else{
                            if(authCode.length() != 6){
                                AlertTool.getAlertDialog(NewUserActivity.this, getString(R.string.title), Constants.MSG_AUTH_CODE_FORMAT_ERR).show();
                            }else{
                                //檢查通過 驗證驗證碼
                                verifyVerificationCode(authCode,mobile,password);
                            }

                        }
                    }
                }
            }
        }
    };


    private void verifyVerificationCode(String code,String mobile,String password) {
        Log.i(TAG,"verifyVerificationCode  authCode is "+authCode + "code is "+code );
        if(authCode != null){
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(authCode, code);
            signInWithPhoneAuthCredential(credential, mobile, password);
        }
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final String mobile, final String password) {
        Log.i(TAG,"signInWithPhoneAuthCredential  authCode is "+authCode);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            register(mobile,password);
                        } else {
                            AlertTool.getAlertDialog(NewUserActivity.this, getString(R.string.title), Constants.MSG_AUTH_CODE_FORMAT_ERR).show();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            }
                        }
                    }
                });
    }

    /**
     * 獲得認證碼
     */
    private void authCode( final String mobile){
        OKHttpHelper.authCode(mobile, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "IOException:", e);
            }
            @Override
            public void onResponse(Call call, Response response){
                if(response.isSuccessful()){
                    try {
                        String responseStr = response.body().string();
                        Log.i(TAG,"responseStr is "+responseStr);
                        JSONObject jsonObject = new JSONObject(responseStr);
                        responseMessage =jsonObject.getString(Constants.JSON_MESSAGE);
                        Log.i(TAG,"responseMessage is "+responseMessage);
                        NewUserActivity.this.runOnUiThread(new Runnable( ) {
                            @Override
                            public void run() {
                                AlertTool.getAlertDialog(NewUserActivity.this,getString(R.string.title),responseMessage).show();
                            }
                        });
                        if (jsonObject.getBoolean(Constants.JSON_SUCCESS)) {
                            Log.i(TAG,"success is true");
                            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                    "+886"+ mobile,
                                    60,
                                    TimeUnit.SECONDS,
                                    TaskExecutors.MAIN_THREAD,
                                    mCallbacks);
                        } else {
                            Log.i(TAG,"success is false");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        });
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks  mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override //送出完成後,收到簡訊碼
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                hasMessageCode = true;
                Log.i(TAG,"Fire Base code is "+code);
                System.out.println( "code "+code );
            }
        }
        @Override// 送出失敗
        public void onVerificationFailed(FirebaseException e) {
           Log.i(TAG,"fireBase is error ",e);
        }

        @Override // 送出當下,收到ID識別號 authCode
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            authCode = s;
        }
    };
    /**
     * 註冊帳號
     */
    private void register(final String mobile, String password){
        OKHttpHelper.register(mobile, password, new Callback( ) {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "IOException:", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    try {
                        String responseStr = response.body().string();
                        Log.i(TAG,"responseStr is "+responseStr);
                        JSONObject jsonObject = new JSONObject(responseStr);
                        final boolean success = jsonObject.getBoolean(Constants.JSON_SUCCESS);
                        responseMessage =jsonObject.getString(Constants.JSON_MESSAGE);
                        Log.i(TAG,"responseMessage is "+responseMessage);
                        NewUserActivity.this.runOnUiThread(new Runnable( ) {
                            @Override
                            public void run() {
                                if (!success) AlertTool.getAlertDialog(NewUserActivity.this,getString(R.string.title),responseMessage).show();
                            }
                        });
                        if (jsonObject.getBoolean(Constants.JSON_SUCCESS)) {

                            Log.i(TAG,"success is true");
                            String m = mobile;
                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            intent.setClass(NewUserActivity.this,UserDetailActivity.class);
                            bundle.putString("mobile",m);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();

                        } else {
                            Log.i(TAG,"success is false");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        });
    }
}
