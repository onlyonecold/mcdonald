package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.databinding.ActivityPasswordMessageBinding;

public class PasswordMessageActivity extends AppCompatActivity {
    private static final String TAG = "PasswordMessageActivity";
    ActivityPasswordMessageBinding binding;
    Button reSingIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPasswordMessageBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        reSingIn = binding.btnReSIngIn;
        reSingIn.setOnClickListener(new Button.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(PasswordMessageActivity.this,SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
