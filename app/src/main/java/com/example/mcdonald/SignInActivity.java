package com.example.mcdonald;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mcdonald.common.Constants;
import com.example.mcdonald.databinding.ActivitySignInBinding;
import com.example.mcdonald.helper.OKHttpHelper;
import com.example.mcdonald.util.AlertTool;
import com.example.mcdonald.util.FaceBookSingIn;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "SignInActivity";
    TextView.OnClickListener newUser = new TextView.OnClickListener() {
        @Override
        public void onClick(View view) {

            Intent intent = new Intent();
            intent.setClass(SignInActivity.this, NewUserActivity.class);
            startActivity(intent);

        }
    };
    TextView.OnClickListener forgetPassword = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setClass(SignInActivity.this, ForgetPasswordActivity.class);
            startActivity(intent);

        }
    };
    Button.OnClickListener back = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
            finish();

        }
    };
    private FaceBookSingIn faceBookSingIn;
    private ActivitySignInBinding binding;
    private String responseMessage;
    private EditText editUser, editPassword;
    Button.OnClickListener chickSingIn = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            String mobile = editUser.getText().toString().trim();
            String password = editPassword.getText().toString().trim();
            if (mobile != "" && password != "") {
                if (mobile.matches(Constants.MOBILE_PHONE_VERIFICATION)) {
                    if (password.matches(Constants.PASSWORD_VERIFICATION)) {
                        singIn(mobile, password);
                    } else {
                        AlertTool.getAlertDialog(SignInActivity.this, getString(R.string.app_name), getString(R.string.passwordFormatErr));
                    }
                } else {
                    AlertTool.getAlertDialog(SignInActivity.this, getString(R.string.app_name), getString(R.string.mobileFormatErr));
                }
            }
        }
    };
    private Button btnSingIn, btnFacebook, btnGoogle;
    private ImageButton btnBack;
    private TextView btnRegister, btnForgetPassword, errUser, errPassword;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleSignInClient googleSignInClient;
    private LoginManager loginManager;
    private CallbackManager callbackManager;
    Button.OnClickListener loginFaceBook = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            initFB();
//            faceBookSingIn.initFB();

        }
    };
    private int RC_SIGN_IN = 0; // 設定變數值
    Button.OnClickListener googleLogin = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            signIn();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignInBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        findView();
        setListener();

        FirebaseInstanceId. getInstance ().getInstanceId().addOnSuccessListener(
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        Log. i ( "MainActivity" , "token " +newToken);
                    }
                } ).addOnFailureListener( new OnFailureListener() {
            @Override
            public void onFailure( @NonNull Exception e) {
                Log. i ( "FireBaseToken" , "onFailure : " + e.toString());
            }
        });



        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();
        faceBookSingIn = new FaceBookSingIn(SignInActivity.this, loginManager, callbackManager);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onSuccess:onActivityResult!");
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (acct != null) {  //登入的帳號不為空
                String personName = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();
                String personId = acct.getId();
                Log.i(TAG, "personName is " + personName + " " +
                        "personGivenName is " + personGivenName +
                        " personFamilyName is " + personFamilyName +
                        " personEmail is " + personEmail +
                        " personId is " + personId);

                startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                finish();
            }
        } catch (ApiException e) {
            Log.e(TAG, "handleSignInResult is error" + e);
        }
    }

    private void findView() {
        btnSingIn = binding.btnSingIn;
        btnRegister = binding.btnRegister;
        btnForgetPassword = binding.btnForgetPassword;
        btnFacebook = binding.btnFacebook;
        btnGoogle = binding.btnGoogle;
        btnBack = binding.btnBack;
        editUser = binding.editUser;
        editPassword = binding.editPassword;
        errUser = binding.errUser;
        errPassword = binding.errPassword;

    }

    private void setListener() {
        btnRegister.setOnClickListener(newUser);
        btnForgetPassword.setOnClickListener(forgetPassword);
        btnSingIn.setOnClickListener(chickSingIn);
        btnBack.setOnClickListener(back);
        btnGoogle.setOnClickListener(googleLogin);
        btnFacebook.setOnClickListener(loginFaceBook);

    }

    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void initFB() {
        // init facebook

        // init LoginManager & CallbackManager
        loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();  //FB condition accessToken
        loginFB();
    }

    private void loginFB() {
        // 設定FB login的顯示方式 ; 預設是：NATIVE_WITH_FALLBACK
        /**
         * 1. NATIVE_WITH_FALLBACK
         * 2. NATIVE_ONLY
         * 3. KATANA_ONLY
         * 4. WEB_ONLY
         * 5. WEB_VIEW_ONLY
         * 6. DEVICE_AUTH
         */
        loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        // 設定要跟用戶取得的權限，以下3個是基本可以取得，不需要經過FB的審核
        List<String> permissions = new ArrayList<>();
        permissions.add("public_profile");
        permissions.add("email");

        // 設定要讀取的權限
        loginManager.logInWithReadPermissions(this, permissions);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // 登入成功
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            if (response.getConnection().getResponseCode() == 200) {
                                long id = object.getLong("id");
                                String name = object.getString("name");
                                String email = object.getString("email");
                                Log.d(TAG, "Facebook id:" + id);
                                Log.d(TAG, "Facebook name:" + name);
                                Log.d(TAG, "Facebook email:" + email);

                                startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                                finish();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // 用戶取消
                Log.d(TAG, "Facebook onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                // 登入失敗
                Log.d(TAG, "Facebook onError:" + error.toString());
            }
        });
    }

    private void singIn(String mobile, String password) {
        OKHttpHelper.singIn(mobile, password, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "IOException:", e);
            }

            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    try {
                        String responseStr = response.body().string();
                        Log.i(TAG, "responseStr is " + responseStr);
                        JSONObject jsonObject = new JSONObject(responseStr);
                        responseMessage = jsonObject.getString(Constants.JSON_MESSAGE);
                        final boolean success = jsonObject.getBoolean(Constants.JSON_SUCCESS);
                        Log.i(TAG, "responseMessage is " + responseMessage);
                        SignInActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!success)
                                    AlertTool.getAlertDialog(SignInActivity.this, getString(R.string.title), responseMessage).show();
                            }
                        });
                        if (jsonObject.getBoolean(Constants.JSON_SUCCESS)) {
                            Intent intent = new Intent();
                            intent.setClass(SignInActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            Log.i(TAG, "success is true");
                        } else {
                            Log.i(TAG, "success is false");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        });
    }
}
